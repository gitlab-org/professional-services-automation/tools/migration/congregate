## Repository Structure

* [Folder Structure](#repository-folder-structure)
* [Repository Files](#repository-files)


### Repository Folder Structure

* [Azure Scripts](azure_scripts)
* [Continuous Integration](ci)
* [Congregate](congregate)
* [Customer](customer)
* [Data](data)
* [Development](dev)
* [Docker](docker)
* [Documentation](docs)
* [Congregate User Interface](frontend)
* [Images](img)
* [NGINX Proxy](nginx_proxy)
* [Runbook's](runbooks)
* [Templates](templates)

### Repository Files

* [Gitlab CI Pipeline](.gitlab-ci.yml)
* [LICENSE](LICENSE)
* [code-quality.html](code-quality.html)
* [congregate.conf.template](congregate.conf.template)
* [mkdocs.yml](mkdocs.yml)
* [poetry.lock](poetry.lock)
* [pyproject.toml](pyproject.toml)
* [congregate.sh](congregate.sh)