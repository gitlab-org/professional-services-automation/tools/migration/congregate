from congregate.migration.ado.api.base import AzureDevOpsApiWrapper


class UsersApi():
    def __init__(self):
        self.api = AzureDevOpsApiWrapper()

    def get_user(self, descriptor):
        """
        Retrieve the user matching the supplied userDescriptor.

        Core REST API: https://learn.microsoft.com/en-us/rest/api/azure/devops/graph/users/get?view=azure-devops-rest-7.1&tabs=HTTP
        """
        return self.api.generate_get_request(f"_apis/graph/users/{descriptor}", sub_api="vssps")
    
    def get_user_by_guid(self, guid):
        """
        Retrieve the ADO user details given a guid.

        Core REST API: https://learn.microsoft.com/en-us/rest/api/azure/devops/graph/users/get?view=azure-devops-rest-7.1&tabs=HTTP
        """
        return self.api.generate_get_request(f"_apis/identities/{guid}", sub_api="vssps")

    def get_all_users(self):
        """
        Retrieve all users.

        Core REST API: https://learn.microsoft.com/en-us/rest/api/azure/devops/graph/users/list?view=azure-devops-rest-7.1&tabs=HTTP
        :param params: (str) Any query parameters needed in the request
        # List of possible subject types (subjectTypes param):
        # AadUser                 = "aad" # Azure Active Directory Tenant
        # MsaUser                 = "msa" # Windows Live
        # UnknownUser             = "unusr"
        # BindPendingUser         = "bnd" # Invited user with pending redeem status
        # WindowsIdentity         = "win" # Windows Active Directory user
        # UnauthenticatedIdentity = "uauth"
        # ServiceIdentity         = "svc"
        # AggregateIdentity       = "agg"
        # ImportedIdentity        = "imp"
        # ServerTestIdentity      = "tst"
        # GroupScopeType          = "scp"
        # CspPartnerIdentity      = "csp"
        # SystemServicePrincipal  = "s2s"
        # SystemLicense           = "slic"
        # SystemScope             = "sscp"
        # SystemCspPartner        = "scsp"
        # SystemPublicAccess      = "spa"
        # SystemAccessControl     = "sace"
        # AcsServiceIdentity      = "acs"
        # Unknown                 = "ukn"
        """

        params = {
            "subjectTypes": "aad,msa"
        }
        return self.api.list_all("_apis/graph/users", sub_api="vssps", params=params)

    def get_group_members(self, group_id):
        """
        Retrieve all group members.
        Core REST API: https://learn.microsoft.com/en-us/rest/api/azure/devops/memberentitlementmanagement/members/get?view=azure-devops-rest-7.1
        """
        return self.api.list_all(f"_apis/GroupEntitlements/{group_id}/members", sub_api="vsaex")