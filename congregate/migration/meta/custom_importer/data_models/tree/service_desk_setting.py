
from dataclasses import dataclass, asdict
from gitlab_ps_utils.dict_utils import strip_none
                    
@dataclass
class ServiceDeskSetting:
    '''
        Currently unused dataclass, but it exists in a standard project export
    '''
    pass
                    