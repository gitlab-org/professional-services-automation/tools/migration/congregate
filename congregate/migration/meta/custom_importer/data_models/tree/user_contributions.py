
from dataclasses import dataclass

@dataclass
class UserContributions:
    '''
        Currently unused dataclass, but it exists in a standard project export
    '''
    pass
                    