
from dataclasses import dataclass

@dataclass
class ErrorTrackingSetting:
    '''
        Currently unused dataclass, but it exists in a standard project export
    '''
    pass
                    